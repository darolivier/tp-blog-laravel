<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [


        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('isReader', function(User $user) {
            $roles = $user->roles;
            $userRole = $roles->pluck('name');
             return in_array('reader', $userRole->toArray());
         });
         
        $this->registerPolicies();
        Gate::define('isAuthor', function(User $user) {
            $roles = $user->roles;
            $userRole = $roles->pluck('name');
             return in_array('author', $userRole->toArray());
         });
         
        $this->registerPolicies();
        Gate::define('isAdmin', function(User $user) {
            $roles = $user->roles;
            $userRole = $roles->pluck('name');
             return in_array('superAdmin', $userRole->toArray());
         });

       
        //
    }
}
