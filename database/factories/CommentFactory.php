<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [

            'message' => fake()->text(25),
            'user_id' => fake()->numberBetween(1, 50),
            'blog_id' => fake()->numberBetween(1, 150),
            'created_at' => now(),
            'updated_at' => now()

        ];
    }
}
