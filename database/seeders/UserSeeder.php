<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
     
      
        DB::table('users')->insert([
            'name' => 'darolivier',
            'email' => 'darolivier@gmail.com',
            'password' => Hash::make('hellohello'),
           
        ]);
        DB::table('users')->insert([
            'name' => 'darolivierReader',
            'email' => 'darolivierReader@gmail.com',
            'password' => Hash::make('hellohello'),
         

        ]);
        DB::table('users')->insert([
            'name' => 'superAdmin',
            'email' => 'darolivierSA@gmail.com',
            'password' => Hash::make('hellohello'),
         

        ]);

        User::factory()->count(100)->create();

    }
}
