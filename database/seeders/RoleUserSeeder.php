<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->truncate();

        $roles = Role::all();

        // Populate the pivot table
        User::all()->each(function ($user) use ($roles) {
            if($user->name === 'darolivier') {
                $user->roles()->attach(Role::firstWhere('name', 'author')->id);
            } elseif($user->name === 'darolivierReader') {
                $user->roles()->attach(Role::firstWhere('name', 'reader')->id);
            }elseif($user->name === 'superadmin') {
                $user->roles()->attach(Role::firstWhere('name', 'superAdmin')->id);
            } else {
                $user->roles()->attach(
                    Role::all()->random(rand(1, 2))->pluck('id')->toArray()
                );
            }
            
        });


       
    }
}
