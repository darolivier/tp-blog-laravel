<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Blog;
use App\Models\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogs')->truncate();
        Blog::factory()->count(150)->create();
        $users = User::all();

    //     // Populate the pivot table
    //    User::all()->each(function ($user) use ($blogs) { 
    //         $user->roles()->attach(
    //             Role::all()->random(rand(1,2))->pluck('id')->toArray()
    //         );
    //     });
    }
}
