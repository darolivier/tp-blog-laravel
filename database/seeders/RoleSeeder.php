<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\UserRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('roles')->truncate();
    DB::table('roles')->insert([
        'name' => 'author',
        'id' => 1,
      
       
    ]);
    DB::table('roles')->insert([
        'name' => 'reader',
        'id' => 2,
      
       
    ]);
    DB::table('roles')->insert([
        'name' => 'superAdmin',
        'id' => 3,
      
       
    ]);



  
   
    }
}
